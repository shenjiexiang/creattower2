﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameWorldManager : MonoBehaviour
{
    private GameObject[] enemySets;
    private int enemyNum;
    private int maxEnemy;
    private float enemyCreatRate;

    public GameObject[] EnemySets { get => enemySets; set => enemySets = value; }
    public int EnemyNum { get => enemyNum; }
    public int MaxEnemy { get => maxEnemy; set => maxEnemy = value; }
    public float EnemyCreatRate { get => enemyCreatRate; set => enemyCreatRate = value; }

    // Start is called before the first frame update
    void Start()
    {
        InitGameWorld();
        //生成敌人
        InvokeRepeating("CreatEnemy", 0.5f, EnemyCreatRate);
    }

    // Update is called once per frame
    void Update()
    {
        if (enemyNum >= MaxEnemy)
        {
            CancelInvoke("CreatEnemy");
        }
    }
    void InitGameWorld()
    {
        EnemySets = GameObject.FindGameObjectsWithTag("EnemySet");
        MaxEnemy = 6;
        EnemyCreatRate = 0.9f;
    }
    void CreatEnemy()
    {
        GameObject Enemy = Resources.Load<GameObject>("Enemy");
        for (int i = 0; i < EnemySets.Length; i++)
        {

            Bounds enemySetBounds = EnemySets[i].GetComponent<BoxCollider>().bounds;
            Enemy.transform.position = SetEnemyPosition(enemySetBounds);
            Instantiate(Enemy, EnemySets[i].transform);

            enemyNum += 1;
        }


    }

    private Vector3 SetEnemyPosition(Bounds enemySetBounds)
    {


        return new Vector3(
                UnityEngine.Random.Range(enemySetBounds.min.x * 3.6f, enemySetBounds.max.x * 3.6f),
                UnityEngine.Random.Range(enemySetBounds.min.y * 3.6f, enemySetBounds.max.y * 3.6f),
                0
        );

    }
}
