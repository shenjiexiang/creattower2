﻿using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using UnityEngine;

namespace CreatTower
{
    public class Enemy : MonoBehaviour
    {
        enum EnemyTargetSelect
        {
            Tower,
            Player,
        }
        // Start is called before the first frame update
        EnemyTargetSelect enemyTarget;
        float speed;

        public float Speed { get => speed; set => speed = value; }
        private EnemyTargetSelect EnemyTarget { get => enemyTarget; set => enemyTarget = value; }

        void Start()
        {

            EnemyTarget = (EnemyTargetSelect)Random.Range(0, 2);
            speed = 0.12f;

            //Debug.Log(EnemyTarget);
        }

        // Update is called once per frame
        void Update()
        {

            switch (EnemyTarget)
            {
                case EnemyTargetSelect.Player:
                    GameObject player = GameObject.FindGameObjectWithTag("Player");

                    MoveTo(player.transform);
                    break;
                case EnemyTargetSelect.Tower:
                    GameObject tower = GameObject.FindGameObjectWithTag("Tower");
                    if (tower != null)
                    {
                        tower = GameObject.FindGameObjectWithTag("Tower");
                    }
                    else
                    {
                        tower = GameObject.FindGameObjectWithTag("Player");
                    }

                    MoveTo(tower.transform);

                    break;
            }

        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                Debug.Log("Hit Player");
            }
            if (other.tag == "Tower")
            {

                Debug.Log("Hit " + other.name);
            }
            if (other.GetComponent<CAOneBulletActicon>() != null)
            {
                other.GetComponent<DistoryThis>().OnDestory();
                //  other.GetComponent<CAOneBulletActicon>().StopMovement(transform);
                //   StartCoroutine(other.GetComponent<CAOneBulletActicon>().OnDestory(0.3f));

                transform.GetComponent<DistoryThis>().OnDestory();
            }
        }

        void MoveTo(Transform target)
        {
            // float step = Speed * Time.deltaTime;
            // transform.position = Vector3.Lerp(transform.position, target.position, step);
            transform.GetComponent<AIDestinationSetter>().target = target;
        }
    }
}