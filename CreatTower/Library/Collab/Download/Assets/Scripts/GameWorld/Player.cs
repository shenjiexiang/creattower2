﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace CreatTower
{
    public class Player : MonoBehaviour
    {
        float xMove;
        float yMove;

        float moveSpeed = 3f;



        // Start is called before the first frame update
        void Start()
        {


        }

        // Update is called once per frame
        void Update()
        {
            Move();


            BuildOneTower();



        }

        private void BuildOneTower()
        {
            if (BaseDate.BuildTower == true)
            {
                BaseDate.CreatTowerNum += 1;
                CreatTowerButtom.CreatATowerInGameWorld(transform.position);
                BaseDate.BuildTower = false;
            }
        }

        void Move()
        {

            if (Input.GetKey(KeyCode.D))
            {
                xMove += moveSpeed;
                transform.position = transform.position + new Vector3(xMove * Time.deltaTime, 0, 0);
                xMove = 0;
            }
            else if (Input.GetKey(KeyCode.A))
            {
                xMove += moveSpeed;
                transform.position = transform.position + new Vector3(-xMove * Time.deltaTime, 0, 0);
                xMove = 0;
            }

            if (Input.GetKey(KeyCode.W))
            {
                yMove += moveSpeed;
                transform.position = transform.position + new Vector3(0, yMove * Time.deltaTime, 0);
                yMove = 0;
            }
            else if (Input.GetKey(KeyCode.S))
            {
                yMove += moveSpeed;
                transform.position = transform.position + new Vector3(0, -yMove * Time.deltaTime, 0);
                yMove = 0;
            }

            BaseDate.PlayerPos = transform.position;

        }
    }
}
