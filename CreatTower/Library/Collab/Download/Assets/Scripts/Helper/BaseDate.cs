using UnityEngine;

namespace CreatTower
{
    class BaseDate
    {
        private static int diamondNum;
        private static Vector3 playerPos;

        private static Vector3 enemyPos;
        static bool buildTower;

        static int creatTowerNum;

        static int playerHP;
        static int enemyHP;

        static int towerHP;

        public static bool BuildTower { get => buildTower; set => buildTower = value; }
        public static int DiamondNum { get => diamondNum; set => diamondNum = value; }
        public static Vector3 PlayerPos { get => playerPos; set => playerPos = value; }
        public static int CreatTowerNum { get => creatTowerNum; set => creatTowerNum = value; }
        public static int TowerHP { get => towerHP; set => towerHP = value; }
        public static int EnemyHP { get => enemyHP; set => enemyHP = value; }
        public static int PlayerHP { get => playerHP; set => playerHP = value; }
        public static Vector3 EnemyPos { get => enemyPos; set => enemyPos = value; }
    }


}
