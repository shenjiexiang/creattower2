using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CreatTower
{
    class InitCreaterPannel : GameManagerCreaterPannel
    {
        private static InitCreaterPannel m_Instance = null;
        public static InitCreaterPannel S
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new InitCreaterPannel();
                }
                return m_Instance;
            }
        }
        public void InItGame()
        {
            //物体绑定
            astarPath = GameObject.Find("GameRoot/A*").GetComponent<AstarPath>();

            TowerPos = GameObject.Find("TowerPos");
            Instantiate(Resources.Load<GameObject>("Tower"), TowerPos.transform);


            DiamondNum = GameObject.Find("Canvas/DiamondText").GetComponent<Text>();
            //初始化数值
            GameManagerCreaterPannel.SelectCompomentCounter = 0;
            BaseDate.DiamondNum = 0;
            BaseDate.CreatTowerNum = -1;
            BaseDate.BuildTower = false;

            //按钮监听
            CreatTowerBtn = GameObject.Find("Canvas/Creat").GetComponent<Button>();
            Button btn = GameManagerCreaterPannel.CreatTowerBtn.GetComponent<Button>();
            btn.onClick.AddListener(UICreaterPannel.OnBttomPress);

            AttackButtoms = new List<Button>();
            GameObject[] AttackButtomsobjs = GameObject.FindGameObjectsWithTag("CA");
            foreach (GameObject b in AttackButtomsobjs)
            {
                var bb = b.GetComponent<Button>();
                AttackButtoms.Add(bb);
            }

            for (int i = 0; i < AttackButtoms.Count; i++)
            {
                Button attackBtn = GameManagerCreaterPannel.AttackButtoms[i].GetComponent<Button>();
                attackBtn.onClick.AddListener(UICreaterPannel.OnCompomentAttackButtomsPress);
            }
            GameManagerCreaterPannel.CreatTowerBtn.GetComponent<Button>().interactable = false;

        }

    }


}
