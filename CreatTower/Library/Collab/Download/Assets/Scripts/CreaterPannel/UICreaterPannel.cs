using UnityEngine;
using UnityEngine.EventSystems;

namespace CreatTower
{
    class UICreaterPannel : GameManagerCreaterPannel
    {
        private static UICreaterPannel m_Instance = null;
        public static UICreaterPannel S
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new UICreaterPannel();
                }
                return m_Instance;
            }
        }



        public static void OnBttomPress()
        {
            CreatTowerButtom.OnBttomPress();
            Debug.Log(BaseDate.DiamondNum);

            // CreatTowerButtom.S.CreatATowerInGameWorld(PlayerPos);

            NewTower();

        }
        public static void OnCompomentAttackButtomsPress()
        {
            switch (EventSystem.current.currentSelectedGameObject.name)
            {
                case "CA1":
                    AttackCompomentDate.S.OnTowerpos = AttackCompomentDate.OnTowerPos.PosOne;
                    OnCompomentAttackButtomsPressHelper(1);


                    break;
                case "CA2":
                    AttackCompomentDate.S.OnTowerpos = AttackCompomentDate.OnTowerPos.PosTwo;
                    OnCompomentAttackButtomsPressHelper(2);



                    break;
                case "CA3":
                    AttackCompomentDate.S.OnTowerpos = AttackCompomentDate.OnTowerPos.PosThree;
                    OnCompomentAttackButtomsPressHelper(3);


                    break;
                case "CA4":
                    AttackCompomentDate.S.OnTowerpos = AttackCompomentDate.OnTowerPos.PosFour;
                    OnCompomentAttackButtomsPressHelper(4);


                    break;
                case "CA5":
                    AttackCompomentDate.S.OnTowerpos = AttackCompomentDate.OnTowerPos.PosFive;
                    OnCompomentAttackButtomsPressHelper(5);


                    break;

            }
        }

    }


}
