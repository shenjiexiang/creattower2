using UnityEngine;

namespace CreatTower
{
    interface IAttackCompomentData
    {
        int attackRange { get; set; }
        int dimondCos { get; set; }

        int attackPower { get; set; }


        string AttackCompomentName { get; set; }

        float compomentHP { get; set; }
        bool shootTriger { get; set; }
        float shootRate { get; set; }
        float bulletSpeed { get; set; }
    }
    class AttackCompomentDate
    {
        public enum AttackWay
        {
            LongShoot,
            NearFight,
            ShortShoot,

        }
        public enum OnTowerPos
        {
            PosOne,
            PosTwo,
            PosThree,
            PosFour,
            PosFive,
        }
        int dimondCos;
        int attackRange;
        int attackPower;
        AttackWay attackway;

        string name;

        float compomentHP;
        bool shootTriger;

        float shootRate;

        OnTowerPos onTowerpos;



        public int DimondCos { get => dimondCos; set => dimondCos = value; }
        public int AttackRange { get => attackRange; set => attackRange = value; }
        public int AttackPower { get => attackPower; set => attackPower = value; }

        public string Name { get => name; set => name = value; }
        public float CompomentHP { get => compomentHP; set => compomentHP = value; }
        public AttackWay Attackway { get => attackway; set => attackway = value; }
        public OnTowerPos OnTowerpos { get => onTowerpos; set => onTowerpos = value; }
        public bool ShootTriger { get => shootTriger; set => shootTriger = value; }
        public float ShootRate { get => shootRate; set => shootRate = value; }
        private static AttackCompomentDate m_Instance = null;

        public static AttackCompomentDate S
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new AttackCompomentDate();

                }
                return m_Instance;
            }
        }



        public void CreatAttackCompoment(GameObject AttackCompoment, GameObject towerPos)
        {
            Transform Tower = towerPos.transform.GetChild(0);
            AttackCompoment.transform.GetChild(0).GetComponent<TMPro.TextMeshPro>().text = name;


            switch (onTowerpos)
            {
                case OnTowerPos.PosOne:
                    Vector3 pos0 = Tower.transform.GetChild(0).position;
                    AttackCompoment.transform.position = pos0;
                    break;
                case OnTowerPos.PosTwo:
                    Vector3 pos1 = Tower.transform.GetChild(1).position;
                    AttackCompoment.transform.position = pos1;
                    break;
                case OnTowerPos.PosThree:
                    Vector3 pos2 = Tower.transform.GetChild(2).position;
                    AttackCompoment.transform.position = pos2;
                    break;
                case OnTowerPos.PosFour:
                    Vector3 pos3 = Tower.transform.GetChild(3).position;
                    AttackCompoment.transform.position = pos3;
                    break;
                case OnTowerPos.PosFive:
                    Vector3 pos4 = Tower.transform.GetChild(4).position;
                    AttackCompoment.transform.position = pos4;
                    break;
            }

        }
        public bool Attack(Transform transform)
        {
            GameObject enemy = GameObject.FindGameObjectWithTag("Enemy");

            if (enemy && Vector3.Distance(enemy.transform.position, transform.position) < AttackRange)
            {
                return true;

            }
            else
            {
                return false;
            }






        }
    }


}
