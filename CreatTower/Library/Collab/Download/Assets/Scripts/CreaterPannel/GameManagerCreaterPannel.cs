﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace CreatTower
{
    public class GameManagerCreaterPannel : MonoBehaviour
    {
        public static AstarPath astarPath;

        private static Text diamondNum;
        private static List<Button> attackButtoms;

        private static Button creatTowerBtn;

        private static int selectCompomentCounter;
        private static GameObject towerPos;
        private static Vector3 playerPos = new Vector3(0, 0, 0);


        #region getset
        public static GameObject TowerPos { get => towerPos; set => towerPos = value; }
        public static int SelectCompomentCounter { get => selectCompomentCounter; set => selectCompomentCounter = value; }

        public static Button CreatTowerBtn { get => creatTowerBtn; set => creatTowerBtn = value; }

        public static Vector3 PlayerPos { get => playerPos; set => playerPos = value; }
        public static Text DiamondNum { get => diamondNum; set => diamondNum = value; }
        public static List<Button> AttackButtoms { get => attackButtoms; set => attackButtoms = value; }
        #endregion



        void Start()
        {
            InitCreaterPannel.S.InItGame();
            //生成钻石
            InvokeRepeating("AddOneDiamond", 1.3f, 1.3f);

        }
        void Update()
        {
            PlayerPos = BaseDate.PlayerPos;

            CreatTowerButtom.DiamonNumValueOfTower = 5;

            //解锁创建条件
            if (SelectCompomentCounter >= 3 && BaseDate.DiamondNum > CreatTowerButtom.DiamonNumValueOfTower)
            {
                Debug.Log("CanCreat");
                CreatTowerBtn.GetComponent<Button>().interactable = true;
                SelectCompomentCounter = 0;
            }
            //满足解锁条件后
            if (SelectCompomentCounter >= 3)
            {
                foreach (Button b in AttackButtoms)
                {
                    b.GetComponent<Button>().interactable = false;
                }
            }
            //UI显示
            DiamondNum.text = "Diamond:\n" + BaseDate.DiamondNum.ToString();

        }

        void AddOneDiamond()
        {
            BaseDate.DiamondNum += 1;
        }
        public static void NewTower()
        {
            SelectCompomentCounter = 0;
            CreatTowerBtn.GetComponent<Button>().interactable = false;
            foreach (Button b in AttackButtoms)
            {
                b.GetComponent<Button>().interactable = true;
            }
            BaseDate.BuildTower = true;

        }


        public static void OnCompomentAttackButtomsPressHelper(int num)
        {
            Button CA = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
            Debug.Log("found CA" + num.ToString());
            //AddCompomentToTower();
            // DateCount();
            SelectCompomentCounter += 1;
            GameObject Tower = GameObject.Find("Tower");
            var AttackCompoment = Instantiate(Resources.Load<GameObject>("CA" + num.ToString()), TowerPos.transform.GetChild(0).GetChild(num - 1).transform);
            AttackCompomentDate.S.Name = "CA" + num.ToString();

            AttackCompomentDate.S.CreatAttackCompoment(AttackCompoment, TowerPos);
            CA.interactable = false;
        }
    }



}
