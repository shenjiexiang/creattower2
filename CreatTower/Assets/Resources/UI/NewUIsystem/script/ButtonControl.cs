﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
namespace CreatTower
{
    public class ButtonControl : MonoBehaviour
    {
        enum ButtonState
        {
            Up,
            Down
        }
        private Button _buttom;
        public int _doubleClick;
        private ButtonState _buttonState;





        // Start is called before the first frame update
        void Start()
        {
            _doubleClick = 0;
            _buttom = transform.GetComponent<Button>();
            //  _buttom.onClick.AddListener(() => ButtonClicked(gameObject.name));
            transform.GetChild(0).GetComponent<Text>().text = gameObject.name;
            _buttonState = ButtonState.Down;
        }

        // Update is called once per frame
        void Update()
        {
            Debug.Log("1231231" + BaseDate.BuildTower);
            if (BaseDate.BuildTower == true)
            {

                _doubleClick = 0;
                if (_buttonState == ButtonState.Up)
                {
                    transform.DOMove(new Vector3(
              transform.position.x,
             transform.position.y - (transform.GetComponent<RectTransform>().sizeDelta.y / 3),
              transform.position.z
         ), 0.3f).SetEase(Ease.Linear);
                    _buttonState = ButtonState.Down;
                }


            }
           
        }

        public void ButtonClicked(string name, int num)
        {
            Debug.Log(_doubleClick);

            _doubleClick += 1;

            if (_doubleClick == 1)
            {
                Debug.Log("found CA" + num.ToString());
                //AddCompomentToTower();
                // DateCount();
                // CreatTower.GameManagerCreaterPannel.SelectCompomentCounter += 1;
                GameObject Tower = GameObject.Find("Tower");
                var AttackCompoment = Instantiate(Resources.Load<GameObject>("CA" + num.ToString()), CreatTower.GameManagerCreaterPannel.TowerPos.transform.GetChild(0).GetChild(num - 1).transform);
                CreatTower.AttackCompomentDate.S.Name = "CA" + num.ToString();
                CreatTower.AttackCompomentDate.S.CreatAttackCompoment(AttackCompoment, CreatTower.GameManagerCreaterPannel.TowerPos);


                bool Double = false;
                foreach (string str in WhatSelect.SlotList)
                {
                    if (str == name)
                    {
                        Double = true;
                    }
                }
                if (!Double)
                {
                    WhatSelect.SlotList.Add(name);
                }

                //选择和取消选择的表现
                #region 选择和取消选择的表现
                //选择：

                transform.DOMove(new Vector3(
                     transform.position.x,
                    transform.position.y + (transform.GetComponent<RectTransform>().sizeDelta.y / 3),
                     transform.position.z
                ), 0.3f).SetEase(Ease.OutBounce);
                _buttonState = ButtonState.Up;

            }

            //取消选择：
            if (_doubleClick >= 2)
            {
                // CreatTower.GameManagerCreaterPannel.SelectCompomentCounter -= 1;
                switch (num)
                {
                    case 1:
                        CreatTower.GameManagerCreaterPannel.TowerPos.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<DistoryThis>().OnDestory();

                        //  CreatTower.AttackCompomentDate.S.CreatAttackCompoment(null, CreatTower.GameManagerCreaterPannel.TowerPos);
                        // CA.transform.GetComponent<ButtonControl>()._doubleClick = 0;
                        break;
                    case 2:
                        CreatTower.GameManagerCreaterPannel.TowerPos.transform.GetChild(0).GetChild(1).GetChild(0).GetComponent<DistoryThis>().OnDestory();

                        //  CreatTower.AttackCompomentDate.S.CreatAttackCompoment(null, CreatTower.GameManagerCreaterPannel.TowerPos);
                        // CA.transform.GetComponent<ButtonControl>()._doubleClick = 0;
                        break;
                    case 3:
                        CreatTower.GameManagerCreaterPannel.TowerPos.transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<DistoryThis>().OnDestory();

                        //  CreatTower.AttackCompomentDate.S.CreatAttackCompoment(null, CreatTower.GameManagerCreaterPannel.TowerPos);
                        // CA.transform.GetComponent<ButtonControl>()._doubleClick = 0;
                        break;
                    case 4:
                        CreatTower.GameManagerCreaterPannel.TowerPos.transform.GetChild(0).GetChild(3).GetChild(0).GetComponent<DistoryThis>().OnDestory();

                        //  CreatTower.AttackCompomentDate.S.CreatAttackCompoment(null, CreatTower.GameManagerCreaterPannel.TowerPos);
                        // CA.transform.GetComponent<ButtonControl>()._doubleClick = 0;
                        break;
                    case 5:
                        CreatTower.GameManagerCreaterPannel.TowerPos.transform.GetChild(0).GetChild(4).GetChild(0).GetComponent<DistoryThis>().OnDestory();

                        //  CreatTower.AttackCompomentDate.S.CreatAttackCompoment(null, CreatTower.GameManagerCreaterPannel.TowerPos);
                        // CA.transform.GetComponent<ButtonControl>()._doubleClick = 0;
                        break;
                }

                transform.DOMove(new Vector3(
                transform.position.x,
               transform.position.y - (transform.GetComponent<RectTransform>().sizeDelta.y / 3),
                transform.position.z
           ), 0.3f).SetEase(Ease.Linear);

                _buttonState = ButtonState.Down;

                // int index = WhatSelect.SlotList.IndexOf(name);
                // WhatSelect.SlotList.RemoveAt(index);
                WhatSelect.SlotList.Remove(name);

                _doubleClick = 0;
            }
            #endregion
        }
    }

}
