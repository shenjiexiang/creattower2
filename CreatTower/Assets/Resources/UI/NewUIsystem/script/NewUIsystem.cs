﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System.Text;

public class NewUIsystem : MonoBehaviour
{
    public GameObject[] _UiCardPrefab;
    public float _UiCardSpacing;
    private List<GameObject> _UiCardPrefabs;
    private int _buttonNum = 5;
    private Vector3 _UiCardPos;

    public List<GameObject> UiCardPrefabs { get => _UiCardPrefabs; set => _UiCardPrefabs = value; }

    // Start is called before the first frame update
    void OnEnable()
    {
        InitUIButtoms();
    }

    private void InitUIButtoms()
    {

      //  transform.GetComponent<Button>().onClick.AddListener(CreateButtonPress);

        UiCardPrefabs = new List<GameObject>();
        for (int i = 0; i < _buttonNum; i++)
        {
            GameObject a = Instantiate(_UiCardPrefab[i], transform);
            a.name = ("Card" + i).ToString();
            UiCardPrefabs.Add(a);
            GameObject lastA;
            UiCardPrefabs[0].transform.position = new Vector3(
                transform.position.x + transform.GetComponent<RectTransform>().sizeDelta.x / 2 + a.transform.GetComponent<RectTransform>().sizeDelta.x / 2 + _UiCardSpacing,
                transform.position.y,
                transform.position.z
                );
            if (UiCardPrefabs.Count != 1)
            {
                lastA = UiCardPrefabs[i - 1];
                UiCardPrefabs[i].transform.position = MoveToRightPlace(lastA, a,i);
            }


        }
    }

    public  void CreateButtonPress()
    {



        transform.GetChild(1).transform.GetComponent<Text>().text = transform.GetComponent<Text>().text + " Has Been Created";

    }

    // Update is called once per frame
    void Update()
    {
        transform.GetComponent<Text>().text = WhatSelect.S.SelectedText();

    }

    private Vector3 MoveToRightPlace(GameObject lastButtomPosition, GameObject currentButtomPosition,int i)
    {
        _UiCardPos = lastButtomPosition.transform.position;
        Vector3 moveto = new Vector3(_UiCardPos.x + _UiCardPrefab[i].GetComponent<RectTransform>().sizeDelta.x + _UiCardSpacing, _UiCardPos.y, _UiCardPos.z);
        return moveto;
    }

}
public sealed class WhatSelect

{

    WhatSelect()
    {
    }
    private static readonly object padlock = new object();
    private static WhatSelect instance = null;
    public static WhatSelect S
    {
        get
        {
            lock (padlock)
            {
                if (instance == null)
                {
                    instance = new WhatSelect();
                }
                return instance;
            }
        }
    }
    string result = "0";
    static List<string> _slotList = new List<string>();


    public static List<string> SlotList { get => _slotList; set => _slotList = value; }



    public string SelectedText()
    {
        // SlotList.RemoveAll(list_item => list_item == null);
        while (SlotList.Remove(null)) ;

        if (SlotList.Count != 0)
        {

            StringBuilder builder = new StringBuilder();

            foreach (string str in SlotList)
            {
                builder.Append(str).Append("|");
            }

            result = builder.ToString();
            return result;
        }

        else
        {
            return "not Select";
        }
    }
}
