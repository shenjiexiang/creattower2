﻿using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using UnityEngine;

namespace CreatTower
{
    public class Enemy : MonoBehaviour
    {
        enum EnemyTargetSelect
        {
            Tower,
            Player,
        }
        // Start is called before the first frame update
        EnemyTargetSelect enemyTarget;
        float speed;

        int attackPower;

        public float Speed { get => speed; set => speed = value; }
        private EnemyTargetSelect EnemyTarget { get => enemyTarget; set => enemyTarget = value; }
        public int AttackPower { get => attackPower; set => attackPower = value; }

        void Start()
        {

            EnemyTarget = (EnemyTargetSelect)Random.Range(0, 2);
            Speed = 1.2f;
            AttackPower = 3;
            //Debug.Log(EnemyTarget);
            transform.GetComponent<AIPath>().maxSpeed = Speed;
        }

        // Update is called once per frame
        void Update()
        {

            switch (EnemyTarget)
            {
                case EnemyTargetSelect.Player:
                    GameObject player = GameObject.FindGameObjectWithTag("Player");

                    MoveTo(player.transform);
                    break;
                case EnemyTargetSelect.Tower:
                    GameObject tower = GameObject.FindGameObjectWithTag("Tower");
                    if (tower != null)
                    {
                        tower = GameObject.FindGameObjectWithTag("Tower");
                    }
                    else
                    {
                        tower = GameObject.FindGameObjectWithTag("Player");
                    }

                    MoveTo(tower.transform);

                    break;
            }

        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.tag == "Player")
            {
                Debug.Log("Hit Player");
                StepBack(other.transform);
            }
            if (other.tag == "Tower")
            {

                Debug.Log("Hit " + other.name);
                StepBack(other.transform);
            }
            if (other.GetComponent<CAOneBulletActicon>() != null)
            {
                other.GetComponent<DistoryThis>().OnDestory();
                //  other.GetComponent<CAOneBulletActicon>().StopMovement(transform);
                //   StartCoroutine(other.GetComponent<CAOneBulletActicon>().OnDestory(0.3f));

                transform.GetComponent<DistoryThis>().OnDestory();
            }
            if (other.tag == "CA")
            {

                if (other.GetComponent<CAOne>() != null)
                {
                    Debug.Log("Hit " + other.name);
                    StepBack(other.transform);
                }
            }
        }

        void MoveTo(Transform target)
        {
            // float step = Speed * Time.deltaTime;
            // transform.position = Vector3.Lerp(transform.position, target.position, step);
            transform.GetComponent<AIDestinationSetter>().target = target;
        }
        void StepBack(Transform target)
        {

            Vector3 thirdTarget = LerpByDistance(target.position, transform.position, 1.2f);
            float a = (transform.position.x + thirdTarget.x) / 2;
            float b = ((transform.position.y + thirdTarget.y) / 2);
            transform.GetChild(0).GetChild(0).transform.position = target.position;
            transform.GetChild(0).GetChild(1).transform.position = new Vector3(a, b, transform.parent.position.z - Mathf.Abs((a + b) * 0.3f));
            transform.GetChild(0).GetChild(2).transform.position = new Vector3(thirdTarget.x, thirdTarget.y, target.position.z);
            GetComponent<ParabolaController>().Speed = speed * Random.Range(1.2f, 1.5f);
            GetComponent<ParabolaController>().FollowParabola();
        }
        public Vector3 LerpByDistance(Vector3 A, Vector3 B, float x)
        {
            Vector3 P = x * Vector3.Normalize(B - A) + A;
            return P;
        }
    }
}