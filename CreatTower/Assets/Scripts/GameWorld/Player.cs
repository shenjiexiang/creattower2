﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace CreatTower
{
    public class Player : MonoBehaviour
    {
        static float xMove;
        static float yMove;

        float moveSpeed = 3f;

        public static bool canMove;

        public static bool bXmoveL, bXmoveR, bYmoveU, bYmoveD;
        public static bool isMoving;

        public static float XMove { get => xMove; set => xMove = value; }
        public static float YMove { get => yMove; set => yMove = value; }

        bool doOnce;

        // Start is called before the first frame update
        void Start()
        {

            doOnce = false;
            canMove = true;

        }

        // Update is called once per frame
        void Update()
        {
            Move();


            BuildOneTower();



        }

        private void BuildOneTower()
        {
            
            if (BaseDate.BuildTower == true)
            {

                canMove = false;
                if (doOnce == false)
                {
                    StartCoroutine(WaitAndPrint(BaseDate.BuildingTime));
                    doOnce = true;
                }
              
              
            }
        }
        private IEnumerator WaitAndPrint(float waitTime)
        {
            
            
            yield return new WaitForSeconds(waitTime);
            BaseDate.CreatTowerNum += 1;
            CreatTowerButtom.CreatATowerInGameWorld(transform.position);
            BaseDate.BuildTower = false;
            canMove = true;
            doOnce = false;

        }
        void Move()
        {
            if (canMove)
            {


                if (Input.GetKey(KeyCode.D))
                {
                    isMoving = true;
                    XMove += moveSpeed;
                    //  transform.Translate(XMove * Time.deltaTime, 0, 0, Space.World);
                    transform.position = transform.position + new Vector3(XMove * Time.deltaTime, 0, 0);
                    bXmoveR = true;
                    bXmoveL = false;
                    bYmoveU = false;
                    bYmoveD = false;

                    XMove = 0;
                }


                if (Input.GetKey(KeyCode.A))
                {
                    isMoving = true;
                    XMove -= moveSpeed;
                    //  transform.Translate(XMove * Time.deltaTime, 0, 0, Space.World);
                    transform.position = transform.position + new Vector3(XMove * Time.deltaTime, 0, 0);
                    bXmoveR = false;
                    bXmoveL = true;
                    bYmoveU = false;
                    bYmoveD = false;

                    XMove = 0;
                }


                if (Input.GetKey(KeyCode.W))
                {
                    isMoving = true;
                    YMove += moveSpeed;
                    //   transform.Translate(0, YMove * Time.deltaTime, 0, Space.World);
                    transform.position = transform.position + new Vector3(0, YMove * Time.deltaTime, 0);
                    bXmoveR = false;
                    bXmoveL = false;
                    bYmoveU = true;
                    bYmoveD = false;

                    YMove = 0;
                }


                if (Input.GetKey(KeyCode.S))
                {
                    isMoving = true;
                    YMove -= moveSpeed;
                    // transform.Translate(0, YMove * Time.deltaTime, 0, Space.World);
                    transform.position = transform.position + new Vector3(0, YMove * Time.deltaTime, 0);

                    bXmoveR = false;
                    bXmoveL = false;
                    bYmoveU = false;
                    bYmoveD = true;

                    YMove = 0;
                }
            }

            if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.A))
            {
                bXmoveR = false;
                bXmoveL = true;
                bYmoveU = true;
                bYmoveD = false;

            }
            if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.D))
            {
                bXmoveR = true;
                bXmoveL = false;
                bYmoveU = true;
                bYmoveD = false;

            }
            if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.A))
            {
                bXmoveR = false;
                bXmoveL = true;
                bYmoveU = false;
                bYmoveD = true;

            }
            if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.D))
            {
                bXmoveR = true;
                bXmoveL = false;
                bYmoveU = false;
                bYmoveD = true;

            }


            if (Input.GetKey(KeyCode.D) == false && Input.GetKey(KeyCode.A) == false && Input.GetKey(KeyCode.W) == false && Input.GetKey(KeyCode.S) == false)
            {
                isMoving = false;
            }



            BaseDate.PlayerPos = transform.position;

        }
    }
}
