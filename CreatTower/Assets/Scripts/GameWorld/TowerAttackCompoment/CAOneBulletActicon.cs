﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CreatTower
{
    public class CAOneBulletActicon : MonoBehaviour
    {
        static float speed;
        static Vector3 enemyPos;
        public static float Speed { get => speed; set => speed = value; }
        public static Vector3 EnemyPos { get => enemyPos; set => enemyPos = value; }
        bool MyFunctionCalled = false;
        private void Init()
        {
            transform.GetChild(0).GetChild(2).transform.position = enemyPos;
            float a = (transform.parent.position.x + enemyPos.x) / 2;
            float b = ((transform.parent.position.y + enemyPos.y) / 2);
            transform.GetChild(0).GetChild(1).transform.position = new Vector3(
                a,
                b,
             transform.parent.position.z - Mathf.Abs((a + b) * 1.3f)
                );
            transform.GetChild(0).GetChild(0).transform.position = transform.parent.position;
            GetComponent<ParabolaController>().Speed = Speed;
        }
        private void Start()
        {
            Init();
            GetComponent<ParabolaController>().FollowParabola();

        }
        private void Update()
        {
            if (GetComponent<ParabolaController>().animationTime == float.MaxValue && MyFunctionCalled == false)
            {
                StartCoroutine(OnDestory(0.3f));

                MyFunctionCalled = true;
            }

        }
        public IEnumerator OnDestory(float destoryDelateTime)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, transform.localScale * 4.2f, 0.12f);
            yield return new WaitForSeconds(destoryDelateTime);
            GetComponent<DistoryThis>().OnDestory();
        }

        // public void StopMovement(Transform target)
        // {
        //     transform.GetChild(0).GetChild(2).transform.position = target.position;
        //     StartCoroutine(OnDestory(0.12f));
        // }
    }
}
