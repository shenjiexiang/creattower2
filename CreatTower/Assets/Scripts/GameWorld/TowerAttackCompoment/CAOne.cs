﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace CreatTower
{
    public class CAOne : MonoBehaviour, IAttackCompomentData
    {
        public int attackRange { get; set; }
        public int dimondCos { get; set; }
        public int attackPower { get; set; }
        public string AttackCompomentName { get; set; }
        public float compomentHP { get; set; }
        public bool shootTriger { get; set; }
        public float shootRate { get; set; }
        public float bulletSpeed { get; set; }


        bool MyFunctionCalled = false;


        // Start is called before the first frame update
        void OnEnable()
        {


            InitCAOne();


            InvokeRepeating("Shoot", shootRate, shootRate);
        }

        void InitCAOne()
        {
            attackRange = 5;
            dimondCos = 3;
            attackPower = 5;
            AttackCompomentName = "CA1";
            compomentHP = 10;
            shootTriger = false;
            shootRate = 1.3f;
            bulletSpeed = 6;

            AttackCompomentDate.S.AttackRange = attackRange;
            CreatTowerButtom.DiamonNumValueOfTower += GameObject.FindObjectOfType<CAOne>().dimondCos;
        }
        void Shoot()
        {

            shootTriger = AttackCompomentDate.S.Attack(transform);
            if (shootTriger == true)
            {
                Instantiate(Resources.Load<GameObject>("CABullet/CA1Bullet"), transform);
                Transform bullet = transform.GetChild(1);
                if (bullet != null)
                {
                    Instantiate(Resources.Load<GameObject>("CABullet/CA1Bullet"), transform);
                    bullet = transform.GetChild(1);

                    GameObject[] enemys = GameObject.FindGameObjectsWithTag("Enemy");

                    Dictionary<float, GameObject> FGameObj = new Dictionary<float, GameObject>();
                    List<float> distanceNums = new List<float>();

                    foreach (GameObject enemy in enemys)
                    {
                        float distance = Vector3.Distance(transform.position, enemy.transform.position);
                        FGameObj.Add(distance, enemy);

                        distanceNums.Add(distance);
                    }

                    float[] distanceNumsArray = distanceNums.ToArray();
                    float distanceKey = distanceNumsArray.Min();

                    if (FGameObj.ContainsKey(distanceKey))
                    {
                        GameObject enemy = FGameObj[distanceKey];
                        Vector3 enemyPos = enemy.transform.position;
                        CAOneBulletActicon.Speed = bulletSpeed;
                        CAOneBulletActicon.EnemyPos = enemyPos;
                    }
                }




            }
        }

        void BeenHit(Transform enemyType)
        {
            compomentHP -= enemyType.GetComponent<Enemy>().AttackPower;
        }

        // Update is called once per frame
        void Update()
        {
            if (transform.parent.parent.tag == "Tower" && MyFunctionCalled == false)
            {


                MyFunctionCalled = true;
            }
            if (compomentHP <= 0)
            {
                transform.GetComponent<DistoryThis>().OnDestory();
            }

        }



    }
}
