using System;
using UnityEngine;

namespace CreatTower
{
    public class CreatTowerButtom : GameManagerCreaterPannel
    {
        private static int m_DiamonNumValueOfTower;
        public static int DiamonNumValueOfTower { get => m_DiamonNumValueOfTower; set => m_DiamonNumValueOfTower = value; }
        private static CreatTowerButtom m_Instance = null;
        public static CreatTowerButtom S
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new CreatTowerButtom();
                }
                return m_Instance;
            }
        }



        public static void OnBttomPress()
        {

        //    BaseDate.DiamondNum -= DiamonNumValueOfTower;
            // Debug.Log(DiamonNumValueOfTower);


        }
        public void TowersCompoment()
        {

        }
        public static void CreatATowerInGameWorld(Vector3 playerPos)
        {
            // GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            // cube.transform.position = playerPos;


            LoadRescouseToGameWorld(playerPos);
            Instantiate(Resources.Load<GameObject>("Tower"), TowerPos.transform);
            //astarPath.Scan();

        }



        private static void LoadRescouseToGameWorld(Vector3 playerPos)
        {
            Transform T = TowerPos.transform.GetChild(0);
            Transform G = GameObject.FindGameObjectWithTag("GameRoot").transform.GetChild(1);

            T.gameObject.tag = "Tower";
            T.SetParent(G);
            G.transform.GetChild(BaseDate.CreatTowerNum).transform.GetComponent<MoveMeToGameWorld>().MoveMe(playerPos);


        }


    }


}
