﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

namespace CreatTower
{
    public class GameManagerCreaterPannel : MonoBehaviour
    {
        public static AstarPath astarPath;

        private static Text diamondNum;
        private static List<Button> attackButtoms;

        private static Button creatTowerBtn;

        private static int selectCompomentCounter;
        private static GameObject towerPos;
        private static Vector3 playerPos = new Vector3(0, 0, 0);

       static int _doubleClick;
        #region getset
        public static GameObject TowerPos { get => towerPos; set => towerPos = value; }
        public static int SelectCompomentCounter { get => selectCompomentCounter; set => selectCompomentCounter = value; }

        public static Button CreatTowerBtn { get => creatTowerBtn; set => creatTowerBtn = value; }

        public static Vector3 PlayerPos { get => playerPos; set => playerPos = value; }
        public static Text DiamondNum { get => diamondNum; set => diamondNum = value; }
        public static List<Button> AttackButtoms { get => attackButtoms; set => attackButtoms = value; }
        #endregion

        void Start()
        {
            InItGame();
            //生成钻石
            InvokeRepeating("AddOneDiamond", 1.3f, 1.3f);

        }
        public static void InItGame()
        {
            //物体绑定
            astarPath = GameObject.Find("GameRoot/A*").GetComponent<AstarPath>();

            TowerPos = GameObject.Find("TowerPos");
            Instantiate(Resources.Load<GameObject>("Tower"), TowerPos.transform);


            DiamondNum = GameObject.Find("Canvas/DiamondText").GetComponent<Text>();
            //初始化数值
            GameManagerCreaterPannel.SelectCompomentCounter = 0;
            BaseDate.DiamondNum = 0;
            BaseDate.CreatTowerNum = -1;
            BaseDate.BuildTower = false;
            BaseDate.BuildingTime = 3.3f;

            //按钮监听
            CreatTowerBtn = GameObject.Find("Canvas/CreatBtn").GetComponent<Button>();
            Button btn = GameManagerCreaterPannel.CreatTowerBtn.GetComponent<Button>();
            btn.onClick.AddListener(UICreaterPannel.OnBttomPressUI);

            AttackButtoms = new List<Button>();
            GameObject[] AttackButtomsobjs = GameObject.FindGameObjectsWithTag("CA");
            foreach (GameObject b in AttackButtomsobjs)
            {
                var bb = b.GetComponent<Button>();
                AttackButtoms.Add(bb);
            }
      
            for (int i = 0; i < AttackButtoms.Count; i++)
            {
               
                Button attackBtn = GameManagerCreaterPannel.AttackButtoms[i].GetComponent<Button>();
                attackBtn.onClick.AddListener(UICreaterPannel.OnCompomentAttackButtomsPressed);
            }
           // GameManagerCreaterPannel.CreatTowerBtn.GetComponent<Button>().interactable = false;

        }

        
        void Update()
        {
            PlayerPos = BaseDate.PlayerPos;

            //CreatTowerButtom.DiamonNumValueOfTower = 5;

            //解锁创建条件
            if (SelectCompomentCounter >= 1 && BaseDate.DiamondNum >= CreatTowerButtom.DiamonNumValueOfTower)
            {
                Debug.Log("CanCreat");
                CreatTowerBtn.GetComponent<Button>().interactable = true;
                SelectCompomentCounter = 0;
            }
            //满足解锁条件后
            if (SelectCompomentCounter >= 3)
            {
                foreach (Button b in AttackButtoms)
                {
                   // b.GetComponent<Button>().interactable = false;
                }
            }
            //UI显示
            DiamondNum.text = "Diamond:\n" + BaseDate.DiamondNum.ToString();

        }

        void AddOneDiamond()
        {
            BaseDate.DiamondNum += 1;
        }
        public static void NewTower()
        {
            SelectCompomentCounter = 0;
          //  CreatTowerBtn.GetComponent<Button>().interactable = false;
            foreach (Button b in AttackButtoms)
            {
                b.GetComponent<Button>().interactable = true;
            }
            BaseDate.BuildTower = true;

        }


        public static void OnCompomentAttackButtomsPressHelper(int num)
        {
            Button CA = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
          
            CA.transform.GetComponent<ButtonControl>().ButtonClicked("Card"+num,num);
            


           
        
            




           // CA.interactable = false;
        }
    }



}
