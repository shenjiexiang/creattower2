﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace CreatTower
{


    public class PlayerView : Player
    {
        Vector3 moveDircetion;
        float Speed;

        float yAngle;

        Animator anim;

        float rotationSpeed;

        bool playRuningAnimation, playBuildAnimation;

        // Start is called before the first frame update
        void Start()
        {
            rotationSpeed = 9;
            yAngle = transform.rotation.y;
            playRuningAnimation = false;
            playBuildAnimation = false;

            anim = transform.GetComponent<Animator>();
            anim.speed = 0.63f;
        }

        // Update is called once per frame
        void Update()
        {

            RotationChecker();
            MoveChecker();
            BuilderChecker();


        }

        private void BuilderChecker()
        {

            if (canMove == false)
            {
                playBuildAnimation = true;


            }
            else
            {
                playBuildAnimation = false;
            }



            if (playBuildAnimation == true)
            {
                anim.SetBool("isBuild", true);
            }
            else
            {
                anim.SetBool("isBuild", false);
            }
        }

        private void MoveChecker()
        {
            if (Player.bYmoveU)
            {
                LookAtQuaternion(0);
            }
            if (Player.bYmoveD)
            {
                LookAtQuaternion(180);
            }

            if (Player.bXmoveL)
            {
                LookAtQuaternion(-114);

            }

            if (Player.bXmoveR)
            {
                LookAtQuaternion(-244);
            }
            if (Player.bXmoveL && Player.bYmoveU)
            {
                LookAtQuaternion(-45);
            }
            if (Player.bXmoveR && Player.bYmoveU)
            {
                LookAtQuaternion(45);
            }
            if (Player.bXmoveL && Player.bYmoveD)
            {
                LookAtQuaternion(-130);
            }
            if (Player.bXmoveR && Player.bYmoveD)
            {
                LookAtQuaternion(-230);
            }
        }

        private void RotationChecker()
        {
            if (isMoving == true)
            {

                playRuningAnimation = true;
                Debug.Log("MOVING");
            }
            else
            {
                playRuningAnimation = false;
            }
            if (playRuningAnimation == true)
            {
                anim.SetBool("isMove", true);
            }
            else
            {
                anim.SetBool("isMove", false);
            }
        }

        void LookAtQuaternion(float targetAngele)
        {
            Quaternion target = Quaternion.Euler(transform.rotation.x, targetAngele, transform.rotation.z);
            transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * rotationSpeed);
        }
    }
}